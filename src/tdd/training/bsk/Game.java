package tdd.training.bsk;

public class Game {
	
	private final int THROWS = 10;
	private Frame[] frameVector;
	private int score = 0;
	private int bonus = 0;
	private int firstBonusThrow = 0;
	private int secondBonusThrow = 0;
	private int strikeCounter = 0;
	private int throwCounter = 0;
	

	/**
	 * It initializes an empty bowling game.
	 */
	public Game() throws BowlingException {
		frameVector = new Frame[THROWS];		
	}

	/**
	 * It adds a frame to this game.
	 * 
	 * @param frame The frame.
	 * @throws BowlingException
	 */
	public void addFrame(Frame frame) throws BowlingException {
		frameVector[throwCounter] = frame;
		throwCounter++;
	}

	/**
	 * It return the i-th frame of this game.
	 * 
	 * @param index The index (ranging from 0 to 9) of the frame.
	 * @return The i-th frame.
	 * @throws BowlingException
	 */
	public Frame getFrameAt(int index) throws BowlingException {
		return frameVector[index];	
	}

	/**
	 * It sets the first bonus throw of this game.
	 * 
	 * @param firstBonusThrow The first bonus throw.
	 * @throws BowlingException
	 */
	public void setFirstBonusThrow(int firstBonusThrow) throws BowlingException {
		bonus += firstBonusThrow;
	}

	/**
	 * It sets the second bonus throw of this game.
	 * 
	 * @param secondBonusThrow The second bonus throw.
	 * @throws BowlingException
	 */
	public void setSecondBonusThrow(int secondBonusThrow) throws BowlingException {
		bonus += secondBonusThrow;
	}

	/**
	 * It returns the first bonus throw of this game.
	 * 
	 * @return The first bonus throw.
	 */
	public int getFirstBonusThrow() {
		return firstBonusThrow;
	}

	/**
	 * It returns the second bonus throw of this game.
	 * 
	 * @return The second bonus throw.
	 */
	public int getSecondBonusThrow() {
		return secondBonusThrow;
	}

	/**
	 * It returns the score of this game.
	 * 
	 * @return The score of this game.
	 * @throws BowlingException
	 */
	public int calculateScore() throws BowlingException {
		int tempScore = 0;
		boolean bonusWithTwoStrikes = false;
		for(int i = 0; i < THROWS; i++) {
			tempScore = frameVector[i].getFirstThrow() + frameVector[i].getSecondThrow();
			if(frameVector[i].getFirstThrow() == 10) {
				strikeCounter++;
			}
			if(frameVector[i].getFirstThrow() == 10 && (i+1) != 10) {
				bonus = (frameVector[i+1].getFirstThrow() + frameVector[i+1].getSecondThrow());
				if(frameVector[i+1].getFirstThrow() == 10 && (i+2) != 10) {
					bonus += frameVector[i+2].getFirstThrow();
				}
				tempScore += bonus;
			}
			if(frameVector[i].getFirstThrow() == 10 && (i+1) == 10) {
				setFirstBonusThrow(getFirstBonusThrow());
				setSecondBonusThrow(getSecondBonusThrow());
				tempScore += bonus;
			}
			
			if(tempScore == 10 && (i+1) != 10) {
				bonus = frameVector[i+1].getBonus();
				tempScore += bonus;
			}
			if(tempScore == 10 && (i+1) == 10) {
				setFirstBonusThrow(getFirstBonusThrow());
				tempScore += bonus;
			}
			score += tempScore;
			if(bonus == 20) {
				bonusWithTwoStrikes = true;
			}
		}
		if(strikeCounter == 10) {
			if(bonusWithTwoStrikes) {
				score = 300;
			}
		}
		return score;
	}

}