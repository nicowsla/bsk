package bsk;

import static org.junit.Assert.*;

import org.junit.Test;

import tdd.training.bsk.Frame;
import tdd.training.bsk.Game;


public class FrameTest {

	@Test
	public void testFrameFirstThrow() throws Exception{
		int firstThrow = 2;
		int secondThrow = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(firstThrow, frame.getFirstThrow());
	}

	@Test
	public void testFrameSecondThrow() throws Exception{
		int firstThrow = 2;
		int secondThrow  = 4;
		Frame frame = new Frame(firstThrow, secondThrow);
		assertEquals(secondThrow, frame.getSecondThrow());
	}
	
	@Test
	public void testTemporaryScore() throws Exception{
		int firstThrow = 2;
		int secondThrow  = 6;
		Frame frame = new Frame(firstThrow, secondThrow);
		int score = firstThrow + secondThrow;
		assertEquals(score, frame.getScore());
	}

	@Test
	public void testGame() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		Frame frameTest = new Frame(1,5);
		assertEquals(frameTest.getScore(), game.getFrameAt(0).getScore());
	}
	
	@Test
	public void testCalculateScore() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(81, game.calculateScore());
	}
	
	@Test
	public void testIsSpare() throws Exception{
		Frame frame = new Frame(1, 9);
		assertEquals(true, frame.isSpare());
	}
	
	@Test
	public void testSpare() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1, 9));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(88, game.calculateScore());
	}
	
	@Test
	public void testIsStrike() throws Exception{
		Frame frame = new Frame(10, 0);
		assertEquals(true, frame.isStrike());
	}
	
	@Test
	public void testStrike() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(94, game.calculateScore());
	}
	
	@Test
	public void testStrikeAndSpare() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(4, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(103, game.calculateScore());
	}
	
	@Test
	public void testMultipleStrikes() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(112, game.calculateScore());
	}
	
	@Test
	public void testMultipleSpares() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(8, 2));
		game.addFrame(new Frame(5, 5));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 6));
		assertEquals(98, game.calculateScore());
	}
	
	@Test
	public void testSpareAsTheLastFrame() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(2, 8));
		int firstBonusThrow = 7;
		game.setFirstBonusThrow(firstBonusThrow);
		assertEquals(90, game.calculateScore());
	}
	
	@Test
	public void testStrikeAsTheLastFrame() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(1, 5));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(7, 2));
		game.addFrame(new Frame(3, 6));
		game.addFrame(new Frame(4, 4));
		game.addFrame(new Frame(5, 3));
		game.addFrame(new Frame(3, 3));
		game.addFrame(new Frame(4, 5));
		game.addFrame(new Frame(8, 1));
		game.addFrame(new Frame(10, 0));
		int firstBonusThrow = 7;
		int secondBonusThrow = 2;
		game.setFirstBonusThrow(firstBonusThrow);
		game.setSecondBonusThrow(secondBonusThrow);
		assertEquals(92, game.calculateScore());
	}
	
	@Test
	public void testBestScore() throws Exception{
		Game game = new Game();
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		game.addFrame(new Frame(10, 0));
		int firstBonusThrow = 10;
		int secondBonusThrow = 10;
		game.setFirstBonusThrow(firstBonusThrow);
		game.setSecondBonusThrow(secondBonusThrow);
		assertEquals(300, game.calculateScore());
	}	
}
